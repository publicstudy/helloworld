# 指定基础镜像，代码依托于jdk8来运行
FROM java:8

# 当前目录下的所有jar拷贝到app.jar
COPY *.jar /app.jar

CMD ["--server.port=8080"]

# 对外暴露端口8080
EXPOSE 8080

# 执行java -jar对当前目录下的app.jar文件
ENTRYPOINT ["java","-jar","/app.jar"]
