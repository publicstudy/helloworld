package com.example.helloworld.controller;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class Json1 {

    /**
     * json报文与@JSONField一致
     */
    //bean关系①：驼峰接收
    @JSONField(name = "op_id")
    private String opId;
    //bean关系②：原值接收
    @JSONField(name = "op_id1")
    private String op_id1;
    //bean关系③：它值接收
    @JSONField(name = "op_id2")
    private String param;

    /**
     * json报文传与@JSONField的驼峰转换
     */
    @JSONField(name = "app_id")
    private String app_id;
    @JSONField(name = "appId2")
    private String appId2;
}
