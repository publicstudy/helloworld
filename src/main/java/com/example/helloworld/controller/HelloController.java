package com.example.helloworld.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {


//    @Autowired
//    StringRedisTemplate redisTemplate;

    @RequestMapping("/hello")
    public String hello() {
//        Long views = redisTemplate.opsForValue().increment("views");
        return "hello ==== " + 222;
    }

    @RequestMapping(value = "/json1", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Json1 json1(@RequestBody Json1 json) {
        System.out.println(json);
        return json;
    }

    /**
     * 为什么用requestJson接?我不想和你的bane属性叫的一样，一样了我也不想改，怎么办?
     * @param requestJson
     * @return
     */
    @RequestMapping(value = "/json2", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Json1 json1(@RequestBody String requestJson) {
        System.out.println("request json:" + requestJson);
        Json1 json1 = JSON.parseObject(requestJson, Json1.class);
        System.out.println(json1);
        return json1;
    }
}
